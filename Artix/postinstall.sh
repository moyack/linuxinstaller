#!/bin/bash

echo -e "Installing wireplumber and setting the sound script..."
yay -S --needed wireplumber && mkdir -p ~/.config/autostart/ && echo "#!/bin/bash
pipewire &
pipewire-pulse &
wireplumber &" > ~/.config/autostart/pipewirestart.sh && sudo chmod +x ~/.config/autostart/pipewirestart.sh

echo "Installing grub-btrfs & timeshift..."
git clone https://github.com/Antynea/grub-btrfs.git && cd grub-btrfs && sudo make OPENRC=true SYSTEMD=false install && sudo rc-update add grub-btrfsd default && sudo rc-service grub-btrfsd start && cd .. rm -rf grub-btrfs 
yay -S --needed timeshift timeshift-autosnap cronie-openrc && sudo timeshift-gtk && sudo rc-update add cronie default && sudo rc-service cronie start

echo "Installing zsh optimizations - PowerLevel10K theme"
yay -S --needed ttf-meslo-nerd-font-powerlevel10k zsh-theme-powerlevel10k-git
echo 'source /usr/share/zsh-theme-powerlevel10k/powerlevel10k.zsh-theme' >>~/.zshrc

echo -e "\nRemember to edit ./zshrc so you can have neofetch showing at konsole start"

read -r -p "Do you want to install Openboard? [Y/n]" response
response=${response,,} # tolower
if [[ $response =~ ^(y| ) ]] || [[ -z $response ]]; then
    yay -S openboard
fi
