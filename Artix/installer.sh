#!/bin/bash

sudo timedatectl set-ntp true
sudo hwclock --systohc

echo -e "\n\nInstalling yay..."
sudo pacman -S --needed git base-devel && git clone https://aur.archlinux.org/yay.git && cd yay && makepkg -si && cd .. && rm -rf ./yay

# Upgrades de pacman.conf file with Universe and Ommniverse repositories if they're not added...

cat /etc/pacman.conf | grep "\[universe\]" >> /dev/null || echo "
[universe]
Server = https://universe.artixlinux.org/\$arch
Server = https://mirror1.artixlinux.org/universe/\$arch
Server = https://mirror.pascalpuffke.de/artix-universe/\$arch
Server = https://artixlinux.qontinuum.space/artixlinux/universe/os/\$arch
Server = https://mirror1.cl.netactuate.com/artix/universe/\$arch
Server = https://ftp.crifo.org/artix-universe/\$arch
Server = https://universe.artixlinux.org/\$arch" | sudo tee -a /etc/pacman.conf

cat /etc/pacman.conf | grep "\[omniverse\]" >> /dev/null || echo "
[omniverse]
Server = https://eu-mirror.artixlinux.org/omniverse/\$arch
Server = https://omniverse.artixlinux.org/\$arch
Server = https://artix.sakamoto.pl/omniverse/\$arch" | sudo tee -a /etc/pacman.conf

yay -Syy

echo -e "\033[1;34mInstalling base Desktop\033[0m"
yay -S --needed xorg sddm sddm-openrc plasma packagekit-qt5 konsole ark kate dolphin dolphin-plugins okular spectacle gwenview firefox thunderbird simplescreenrecorder kdenlive vlc exfat-utils neofetch libreoffice-fresh libreoffice-fresh-es hunspell-es_co hyphen-es mythes-es gimp krita inkscape print-manager kio-extras kdegraphics-thumbnailers ffmpegthumbs unrar unarchiver p7zip ladspa rubberband sox movit opencv rtaudio mlt sdl_image qalculate-qt5 qbittorrent-enhanced partitionmanager python python-pip ntpdate

clear
read -r -p "Do you want to install Epson drivers? [Y/n]" response
response=${response,,} # tolower
if [[ $response =~ ^(y| ) ]] || [[ -z $response ]]; then
    echo -e "\033[1;34mInstalling epson drivers...\033[0m"
    yay -S --noconfirm --needed epson-inkjet-printer-escpr
fi

clear
read -r -p "Do you want to install Dropbox? [Y/n]" response
response=${response,,} # tolower
if [[ $response =~ ^(y| ) ]] || [[ -z $response ]]; then
    echo -e "\033[1;34mInstalling Dropbox...\033[0m"
    yay -S --noconfirm --needed dropbox
fi

clear
read -r -p "Do you want to install ZRAM? [Y/n]" response
response=${response,,} # tolower
if [[ $response =~ ^(y| ) ]] || [[ -z $response ]]; then
    echo -e "\033[1;34mInstalling ZRAMD...\033[0m"
    yay -S --noconfirm zramd zram-openrc && sudo rc-update add zram default && sudo rc-service zram start
fi

clear

read -r -p "Do you want to install inxi and downgrade? [Y/n]" response
response=${response,,} # tolower
if [[ $response =~ ^(y| ) ]] || [[ -z $response ]]; then
    echo -e "\033[1;34mInstalling inxi and downgrade...\033[0m"
    yay -S --needed --noconfirm inxi downgrade
fi

clear
read -r -p "Do you want to install Dark Matter grub theme installer? [Y/n]" response
response=${response,,} # tolower
if [[ $response =~ ^(y| ) ]] || [[ -z $response ]]; then
    git clone --depth 1 https://gitlab.com/VandalByte/darkmatter-grub-theme.git && cd darkmatter-grub-theme && sudo python3 darkmatter-theme.py --install && cd .. && rm -rf ./darkmatter-grub-theme
fi

sudo rc-update add sddm default

clear
read -r -p "Do you want to install OhMyZsh? [Y/n]" response
response=${response,,} # tolower
if [[ $response =~ ^(y| ) ]] || [[ -z $response ]]; then
    echo -e "\033[1;34mInstalling OhMyZsh...\033[0m"
    sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
fi

clear
read -r -p "Do you want to reboot now?" response
response=${response,,} # tolower
if [[ $response =~ ^(y| ) ]] || [[ -z $response ]]; then
    sudo reboot
fi
