#!/bin/bash

# This is the preliminary script to install arch (part 2)...

# Now this code is the main thing to do...
clear
read -p "Which is the name of this new system? " pcname
echo -e "This PC is going to be called '$pcname'\n"
ln -sf /usr/share/zoneinfo/America/Bogota /etc/localtime
hwclock --systohc

clear
echo -e "Editing locale.gen...\n"
nano /etc/locale.gen
locale-gen

clear
echo -e "Editing pacman.conf to make the basic adjustements...\n"
nano /etc/pacman.conf
echo "LANG=es_CO.UTF-8" >> /etc/locale.conf
echo "$pcname" >> /etc/hostname
echo "127.0.0.1 localhost" >> /etc/hosts
echo "::1       localhost" >> /etc/hosts
echo "127.0.1.1 $pcname.localdomain $pcname" >> /etc/hosts

clear
read -r -p "Do you want to set root password [y/N]" response
response=${response,,} # tolower
if [[ $response =~ ^(n| ) ]] || [[ -z $response ]]; then
    echo -e "Skipping root password...\n"
else
    echo -e "Setting root password...\n"
    passwd
fi


clear
echo -e "Installing more important stuff...\n"
pacman -Q | grep linux-zen >> /dev/null && linuxk="linux-zen" || linuxk="linux"
pacman -S --needed grub efibootmgr networkmanager network-manager-applet dialog wpa_supplicant mtools dosfstools base-devel $linuxk-headers avahi xdg-user-dirs xdg-utils gvfs bluez bluez-utils cups alsa-utils pipewire pipewire-alsa pipewire-pulse pipewire-jack zsh zsh-completions openssh rsync acpi sof-firmware acpid os-prober ntfs-3g terminus-font usbutils pciutils

clear
graphic=$(lspci | grep "VGA compatible controller")
echo -e "According to lspci, your graphic card is:\n$graphic"
echo "$graphic" | grep NVIDIA >> /dev/null && graphicset="nvidia"
echo "$graphic" | grep AMD >> /dev/null && graphicset="amd"
echo "$graphic" | grep Intel >> /dev/null && graphicset="intel"
echo "Installing driver for $graphicset..."
if [ $graphicset == "amd" ]; then
    pacman -S --noconfirm xf86-video-amdgpu
fi
if [ $graphicset == "nvidia" ]; then
    pacman -S --noconfirm nvidia nvidia-utils nvidia-settings
fi

clear
echo -e "====================================================="
lsblk
echo -e "=====================================================\n"
read -p "According to this, where is the boot path? " bootp

echo -e "====================================================="
efibootmgr
echo -e "=====================================================\n"
read -p "According to this, which bootloader-id should use [GRUB]? " bootn
bootn=${bootn:-GRUB}
grub-install --target=x86_64-efi --efi-directory=$bootp --bootloader-id=$bootn
grub-mkconfig -o /boot/grub/grub.cfg

systemctl enable NetworkManager
systemctl enable bluetooth
systemctl enable cups.service
systemctl enable sshd

read -r -p "Is the installation disk a SSD? [Y/n]" response
response=${response,,} # tolower
if [[ $response =~ ^(y| ) ]] || [[ -z $response ]]; then
    systemctl enable fstrim.timer
fi
systemctl enable acpid

clear
read -p "Who will use this system? " user
useradd -m $user
echo -e "Password for $user...\n"
passwd $user
usermod -aG wheel $user 
echo -e "Adding wheel to sudo...\n"
EDITOR=nano visudo

printf "\e[1;32mDone! Type exit, umount -a and reboot.\e[0m"
echo -e "\n\n"
