#!/bin/bash

lscpu | grep Intel >> /dev/null && proc="intel" || proc="amd"
echo $(lscpu | grep ID)
graphic=$(lspci | grep "VGA compatible controller")
echo -e "According to lspci, your graphic card is:\n$graphic"
echo "$graphic" | grep NVIDIA >> /dev/null && graphicset="nvidia"
echo "$graphic" | grep AMD >> /dev/null && graphicset="amd"
echo "$graphic" | grep Intel >> /dev/null && graphicset="intel"


if [[ $proc == $graphicset ]] || [[ $graphicset != "nvidia" ]]; then
    linuxk="linux-zen"
else
    linuxk="linux"
fi

read -r -p "Do you want to install timeshift? [Y/n]" response
response=${response,,} # tolower
if [[ $response =~ ^(y| ) ]] || [[ -z $response ]]; then
    yay -S --needed --noconfirm timeshift timeshift-autosnap grub-btrfs && sudo timeshift-gtk && sudo systemctl enable --now cronie.service
    echo -e "Editing mkinitcpio.conf...\n"
    sudo nano /etc/mkinitcpio.conf
    sudo mkinitcpio -p $linuxk
fi

clear
echo "Installing zsh optimizations - PowerLevel10K theme"
yay -S --needed --noconfirm ttf-meslo-nerd-font-powerlevel10k zsh-theme-powerlevel10k-git
echo 'source /usr/share/zsh-theme-powerlevel10k/powerlevel10k.zsh-theme' >> ~/.zshrc

clear
echo -e "\nRemember to edit ./zshrc so you can have neofetch showing at konsole start"

read -r -p "Do you want to install Openboard? [Y/n]" response
response=${response,,} # tolower
if [[ $response =~ ^(y| ) ]] || [[ -z $response ]]; then
    pacman -U --noconfirm https://archive.archlinux.org/packages/q/qt5-webkit/qt5-webkit-5.212.0alpha4-18-x86_64.pkg.tar.zst
    yay -S --noconfirm openboard
fi
