#!/bin/bash

sudo timedatectl set-ntp true
sudo hwclock --systohc

sudo pacman -S --noconfirm reflector && sudo systemctl enable reflector.timer && sudo reflector --verbose --country "CO,EC,BR,US,Worldwide" --age 12 --latest 50 --connection-timeout 1 --download-timeout 1 --sort rate --save /etc/pacman.d/mirrorlist

echo -e "\n\nInstalling yay..."
sudo pacman -S --needed git base-devel && git clone https://aur.archlinux.org/yay.git && cd yay && makepkg -si && cd .. && rm -rf ./yay

echo -e "\033[1;34mInstalling base Desktop\033[0m"
yay -S --needed xorg sddm plasma packagekit-qt6 konsole ark kate dolphin dolphin-plugins okular spectacle gwenview kimageformats firefox speech-dispatcher thunderbird simplescreenrecorder kdenlive vlc exfat-utils fastfetch libreoffice-fresh libreoffice-fresh-es hunspell-es_co hyphen-es mythes-es gimp krita inkscape print-manager kio-extras kdegraphics-thumbnailers ffmpegthumbs unrar unarchiver 7zip ladspa rubberband sox movit opencv rtaudio mlt mediainfo sdl_image qalculate-qt qbittorrent partitionmanager python python-pip python-srt python-vosk plymouth plymouth-kcm breeze-plymouth

clear
read -r -p "Do you want to install Epson drivers? [Y/n]" response
response=${response,,} # tolower
if [[ $response =~ ^(y| ) ]] || [[ -z $response ]]; then
    echo -e "\033[1;34mInstalling epson drivers...\033[0m"
    yay -S --noconfirm --needed epson-inkjet-printer-escpr epson-printer-utility skanlite && sudo systemctl start ecbd.service
fi

clear
read -r -p "Do you want to install Dropbox? [Y/n]" response
response=${response,,} # tolower
if [[ $response =~ ^(y| ) ]] || [[ -z $response ]]; then
    echo -e "\033[1;34mInstalling Dropbox...\033[0m"
    yay -S --noconfirm --needed dropbox
fi

clear
read -r -p "Do you want to install ZRAM? [Y/n]" response
response=${response,,} # tolower
if [[ $response =~ ^(y| ) ]] || [[ -z $response ]]; then
    echo -e "\033[1;34mInstalling ZRAMD...\033[0m"
    yay -S --noconfirm zramd && sudo systemctl enable --now zramd.service
fi

clear

read -r -p "Do you want to install inxi and downgrade? [Y/n]" response
response=${response,,} # tolower
if [[ $response =~ ^(y| ) ]] || [[ -z $response ]]; then
    echo -e "\033[1;34mInstalling inxi and downgrade...\033[0m"
    yay -S --noconfirm --needed inxi downgrade
fi

sudo systemctl enable sddm

clear
read -r -p "Do you want to install Titus grub theme installer? [Y/n]" response
response=${response,,} # tolower
if [[ $response =~ ^(y| ) ]] || [[ -z $response ]]; then
    echo -e "\033[1;34minstalling Titus grub theme installer...\033[0m"
    git clone https://github.com/ChrisTitusTech/Top-5-Bootloader-Themes && cd Top-5-Bootloader-Themes && sudo ./install.sh && cd .. & rm -rf ./Top-5-Bootloader-Themes
fi

clear
read -r -p "Do you want to install Dark Matter grub theme installer? [Y/n]" response
response=${response,,} # tolower
if [[ $response =~ ^(y| ) ]] || [[ -z $response ]]; then
    git clone --depth 1 https://gitlab.com/VandalByte/darkmatter-grub-theme.git && cd darkmatter-grub-theme && sudo python3 darkmatter-theme.py --install && cd .. && rm -rf ./darkmatter-grub-theme
fi

clear
read -r -p "Do you want to install OhMyZsh? [Y/n]" response
response=${response,,} # tolower
if [[ $response =~ ^(y| ) ]] || [[ -z $response ]]; then
    echo -e "\033[1;34mInstalling OhMyZsh...\033[0m"
    sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
fi
