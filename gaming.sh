#!/bin/bash

# for video to improve this script: https://youtu.be/v9tb1gTTbJE


lscpu | grep Intel >> /dev/null && proc="intel" || proc="amd"
echo $(lscpu | grep ID)
graphic=$(lspci | grep "VGA compatible controller")
echo -e "According to lspci, your graphic card is:\n$graphic"
echo "$graphic" | grep NVIDIA >> /dev/null && graphicset="nvidia"
echo "$graphic" | grep AMD >> /dev/null && graphicset="amd"
echo "$graphic" | grep Intel >> /dev/null && graphicset="intel"

echo -e "Processor: $proc\nGraphic card: $graphicset\n\n"

# First, configure pacman.conf to enable multilib
sudo nano /etc/pacman.conf
sudo pacman -Syu
echo -e "\n\n installing $graphicset drivers...\n"
# NVIDIA script
if [[ $graphicset == "nvidia" ]]; then
    yay -S --needed --noconfirm nvidia nvidia-utils lib32-nvidia-utils nvidia-settings vulkan-icd-loader lib32-vulkan-icd-loader
fi
# AMD script
if [[ $graphicset == "amd" ]]; then
    yay -S --needed --noconfirm lib32-mesa vulkan-radeon lib32-vulkan-radeon vulkan-icd-loader lib32-vulkan-icd-loader
fi
# INTEL script
if [[ $graphicset == "intel" ]]; then
    yay -S --needed --noconfirm lib32-mesa vulkan-intel lib32-vulkan-intel vulkan-icd-loader lib32-vulkan-icd-loader
fi
clear
echo -e "Installing basic game stuff...\n"
yay -S --needed --noconfirm steam mangohud lib32-mangohud goverlay

clear
read -r -p "Do you want to install Lutris? [Y/n]" response
response=${response,,} # tolower
if [[ $response =~ ^(y| ) ]] || [[ -z $response ]]; then
    yay -S --noconfirm lutris
fi

clear
read -r -p "Do you want to install Heroic Games Launcher? [Y/n]" response
response=${response,,} # tolower
if [[ $response =~ ^(y| ) ]] || [[ -z $response ]]; then
    yay -S --noconfirm heroic-games-launcher
fi

clear
read -r -p "Do you want to install Unigine GPU benchmarks? [Y/n]" response
response=${response,,} # tolower
if [[ $response =~ ^(y| ) ]] || [[ -z $response ]]; then
    yay -S --noconfirm unigine-superposition unigine-valley
fi
