#!/bin/bash

# This is the preliminary script to install arch (part 1)...
# remember to start the script as super user using the command "sudo su"
# Additionally please have the partitions already done. You can use the command 'gfdisk /dev/...'

clear
lscpu | grep Intel >> /dev/null && proc="intel" || proc="amd"
echo $(lscpu | grep ID)
graphic=$(lspci | grep "VGA compatible controller")
echo -e "According to lspci, your graphic card is:\n$graphic"
echo "$graphic" | grep NVIDIA >> /dev/null && graphicset="nvidia"
echo "$graphic" | grep AMD >> /dev/null && graphicset="amd"
echo "$graphic" | grep Intel >> /dev/null && graphicset="intel"

echo -e "Processor: $proc\nGraphic card: $graphicset\n\n"

lsblk

echo -e "\nPlease copy the partition only without the '/dev/'' part of the path..."
read -p "path for the main partition: " mainp
read -p "path for the boot partition: " bootp
read -p "path for the home partition (ENTER if you want to be installed in the main partition): " homep

clear
echo -e "The installation will be in the following way:\n - Boot partition: /dev/$bootp\n - Main partition: /dev/$mainp"
if [ -z "${homep}" ]; then
    echo " - Home partition: <inside main partition>"
else
    echo " - Home partition: /dev/$homep"
fi

read -r -p "Is this device dual boot with Windows? [y/N]" response
response=${response,,} # tolower
if [[ $response =~ ^(n| ) ]] || [[ -z $response ]]; then
    winboot=""
else
    winboot="/efi"
fi

clear
echo -e "We have all the basic information to start the installation.\n"
read -r -p "Are you sure? [Y/n]" response
 response=${response,,} # tolower
 if [[ $response =~ ^(y| ) ]] || [[ -z $response ]]; then
    if [[ $winboot == "" ]]; then
        mkfs.fat -F32 /dev/$bootp
    fi
    mkfs.btrfs -f /dev/$mainp
    mount /dev/$mainp /mnt
    btrfs subvolume create /mnt/@
    if [ -z "${homep}" ]; then
        btrfs subvolume create /mnt/@home
    else
        mkfs.btrfs -f /dev/$homep
    fi
    umount /mnt

    clear
    read -r -p "Is the device /dev/$mainp in a SSD disk? [Y/n]" response
    response=${response,,} # tolower
    if [[ $response =~ ^(y| ) ]] || [[ -z $response ]]; then
        mount -o noatime,ssd,compress=zstd,discard=async,space_cache=v2,subvol=@ /dev/$mainp /mnt
    else
        mount -o subvol=@ /dev/$mainp /mnt
    fi
    mkdir -p /mnt/{home,boot$winboot}
    if [ -z "${homep}" ]; then
        # if $homep path is in the same disk as $mainp, then the SDD verification remains...
        if [[ $response =~ ^(y| ) ]] || [[ -z $response ]]; then
            mount -o noatime,ssd,compress=zstd,discard=async,space_cache=v2,subvol=@home /dev/$mainp /mnt/home 
        else
            mount -o compress=zstd,subvol=@home /dev/$mainp /mnt/home
        fi
    else
        mount /dev/$homep /mnt/home
    fi
    mount /dev/$bootp /mnt/boot$winboot

    clear
    read -r -p "Do you want to adjust pacman.conf? [Y/n]" response
    response=${response,,} # tolower
    if [[ $response =~ ^(y| ) ]] || [[ -z $response ]]; then
        nano /etc/pacman.conf
    fi
    clear
    echo -e "\n~~ Updating pacman keyring... ~~\n"
    pacman -Syy
    pacman -S --noconfirm archlinux-keyring

    clear
    echo -e "\n~~ Installing the basic files into the new partitions... ~~\n"
    if [[ $proc == $graphicset ]] || [[ $graphicset != "nvidia" ]]; then
        linuxk="linux-zen"
    else
        linuxk="linux"
    fi
    pacstrap /mnt --needed base $linuxk $linuxk-headers linux-firmware nano git $proc-ucode btrfs-progs pacman-contrib

    clear
    echo -e "\n~~ Copying partition table into the new system... ~~\n"
    genfstab -U /mnt >> /mnt/etc/fstab
    cat /mnt/etc/fstab

    echo -e "\n~~ Entering into the new system... ~~\n"
    arch-chroot /mnt/
 fi
